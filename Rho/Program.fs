﻿module Rho.Program

module Runners =
    open FParsec

    let testWithState name parser input state =
        match runParserOnString parser state name input with
        | Success(result, _, _)   ->
            printfn "Success:\n%A" result
            Some(result)

        | Failure(errorMsg, _, _) ->
            printfn "Failure: %s" errorMsg
            None

    let test p str = testWithState "default" p str ()

module Ast =
    type Position = FParsec.Position

    type Node<'a> = Node of Value: 'a * Position: Position    

    type LInt = LInt of Node<int>

    type LFloat = LFloat of Node<float>

    type LString = LString of Node<string>

    type LAtom =
         | AInt of LInt
         | AFloat of LFloat
         | AString of LString

    type Def = Def of Ident: LString * Value: LAtom

    type NDef = NDef of Node<Def>

module Parser =
    open FParsec
    open Ast

    type UserState = unit
    type Parser<'a> = Parser<'a, UserState>

    let isLoggingEnabled = false

    let (<!>) (p: Parser<_,_>) label : Parser<_,_> =
        fun stream ->
            if isLoggingEnabled then
                printfn "%A: Entering %s" stream.Position label
            
            let reply = p stream
            
            if isLoggingEnabled then
                printfn "%A: Leaving %s (%A)" stream.Position label reply.Status
            
            reply

    let str s = pstring s

    let ws = spaces

    let ws1 = spaces1

    let pos = getPosition

    let pretag ctor =
        fun (pos: Position, x) -> ctor(x, pos)

    let tag ctor parser =
        pos .>>. parser
        |>> pretag ctor

    let ntag ctor = tag (fun (x, pos) -> ctor (Node(x, pos)))

    let retag ctor parser =
        parser |>> ctor

    let ident: Parser<_> =
        let isValidFirstChar c = isLetter c || c = '_'
        let isValidChar c = isLetter c || isDigit c || c = '_'
        
        many1Satisfy2L isValidFirstChar isValidChar "identifier"
        .>> ws // skips trailing whitespace
        |> ntag LString
        <!> "ident"

    let stringLit: Parser<_> =
        let normalChar = satisfy (fun c -> c <> '\\' && c <> '"')
        let unescape c = match c with
                         | 'n' -> '\n'
                         | c   -> c
        let escapedChar = pstring "\\" >>. (anyOf "\\n\"" |>> unescape)

        between (pstring "\"") (pstring "\"")
            (manyChars (normalChar <|> escapedChar))
        |> ntag LString


    let floatLit = pfloat |> ntag LFloat <!> "float"
    
    let atomLit =
        choice [
            stringLit |> retag AString;
            floatLit |> retag AFloat
        ]

    let def =
        pipe2
            ((ident .>> ws) .>> pchar '=')
            ((ws >>. atomLit) .>> (skipNewline <|> eof))
            (fun ident' atom' -> Def(ident', atom'))
        |> ntag NDef
        <!> "def"

    let statement = def <!> "statement"

    let program = many1 statement <!> "program"


module Executor =
    let ignore ast = ()
    

let input =
    @"
    trait Add 'a =
      add 'a 'a = 'a

    x = add 1 2
    "

let readLines () =
    let rec f lines =
        let line = System.Console.ReadLine()
        
        if (System.String.IsNullOrEmpty(line)) then
            lines
        else
            f (line :: lines)
    f []


let readInput () =
    let lines = readLines ()
    lines |> List.rev |> String.concat "\n"


[<EntryPoint>]
let main argv = 
    let rec f () =
        let input = readInput ()
        
        if System.String.IsNullOrEmpty(input) then
            f()
        else

        Runners.test Parser.program input
        |> Option.map Executor.ignore
        |> ignore

        f()    

    f ()
    0
